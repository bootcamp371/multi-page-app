import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
  selector: 'app-form-exercise',
  templateUrl: './form-exercise.component.html',
  styleUrls: ['./form-exercise.component.css']
})
export class FormExerciseComponent {
  //contacts: Array<Contact> = [];
  newContact:Contact = new Contact("", "", "");
  
  constructor() {
    //this.contacts = [new Contact("", "", "")];
  }

  onSubmit() {
    console.log(`Saving user: ${JSON.stringify(this.newContact)}`);
  }

}
