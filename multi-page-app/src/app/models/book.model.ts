export class Modelsbook {
    title: string;
    author: string;
    ISBN: number;

    constructor(title: string, author: string, ISBN: number) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        }

}
