import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UserInputComponent } from './user-input/user-input.component';
import { WordCounterComponent } from './word-counter/word-counter.component';
import { MathCalculatorComponent } from './math-calculator/math-calculator.component';
import { OutdoorActivitiesComponent } from './outdoor-activities/outdoor-activities.component';
import { LibraryComponent } from './library/library.component';
import { FormExerciseComponent } from './form-exercise/form-exercise.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "products", component: ProductsComponent },
  { path: "contact-us", component: ContactUsComponent },
  { path: "user-input", component: UserInputComponent },
  { path: "word-counter", component: WordCounterComponent },
  { path: "math-calculator", component: MathCalculatorComponent },
  { path: "outdoor-activities", component: OutdoorActivitiesComponent },
  { path: "library", component: LibraryComponent },
  { path: "form-exercise", component: FormExerciseComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ContactUsComponent,
    UserInputComponent,
    WordCounterComponent,
    MathCalculatorComponent,
    OutdoorActivitiesComponent,
    LibraryComponent,
    FormExerciseComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
