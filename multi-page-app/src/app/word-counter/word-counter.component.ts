import { Component } from '@angular/core';

@Component({
  selector: 'app-word-counter',
  templateUrl: './word-counter.component.html',
  styleUrls: ['./word-counter.component.css']
})
export class WordCounterComponent {
  wordCount: string = "";
  words: any;
  onFormSubmitted($event: Event ) : void {
    
    // form submit logic here
    }
    
    wordCounter() {
      this.words = this.wordCount.split(/\s+/).length;
      
    }
}
