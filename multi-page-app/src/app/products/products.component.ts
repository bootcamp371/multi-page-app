import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  message:string = "Under Construction";
  img_width = 400;
  products: Array<any> = [
    {imageURL: "assets/images/rapidBlue_Big.PNG",
    imageAlt:  "Rapid Blue Camaro"
    },
    {imageURL: "assets/images/orange_Big.PNG",
    imageAlt:  "Orange Camaro"
    },
    {imageURL: "assets/images/red_Big.PNG",
    imageAlt:  "Red Camaro"
    }
  ];

}
