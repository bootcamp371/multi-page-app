import { Component } from '@angular/core';
import { Modelsbook } from '../models/book.model';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})

export class LibraryComponent {

  books: Array<Modelsbook> = [];
  constructor() {
    this.books = [new Modelsbook("Misery", "Steven King", 5155522347),
    new Modelsbook("Cujo", "Steven King", 8452255518)]

    this.books.push(new Modelsbook("IT", "Steven King", 6424141558))
  }

}
