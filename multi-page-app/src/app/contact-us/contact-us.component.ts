import { Component } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {
message:string = "Under Construction";

name: string = "Mark Galasso";
email: string = "mark.galasso@centene.com";
phone: string = "248-840-0458";

}
